import { reactive, toRefs } from 'vue';

function testFn() {
  let state99 = reactive({
    TestToRef: '测试toRef',
    TestToRef2: '测试toRef2',
    TestToRef3: '测试toRef3',
  });

  function setTestToRef() {
    console.log(123, '========================');

    state99.TestToRef = 'xxxxxxxx';
  }

  return { ...toRefs(state99), setTestToRef };
}

export default testFn;
