/*
类型断言（Type Assertion）可以用来手动指定一个值的类型

两种语法（第二种兼容性更好）:　
    <类型>值
　　值 as 类型

*/
let someValue: any = "this is a string";

let strLength: number = (someValue as string).length;
let strLength2: number = (<string>someValue).length;
