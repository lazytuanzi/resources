let num: number = 99;
// let str1: string = "jiujiu22";   //与2.xx中冲突 ，从此证明是全局变量
let str2: string = "jiujiu22";
let bool: boolean = false;

//数组
let boolArray: boolean[];
boolArray = [false, true];

//定义接口
interface Name {
  first: string;
  second: string;
  nul: null;
  undef: undefined;
  is?: Boolean; // 可选可不选参数
  [propname: string]: any; //  自定义属性
}

// 使用接口
let name2: Name;
name2 = {
  first: "John",
  second: "Doe",
  nul: null,
  undef: undefined,
  is: false,
  myObj: { q: 1, w: "ww" },
};

// 函数
let fn = (a: number): number => {
  // (a: 实参注解 ): 返回值注解
  console.log(a);
  return a;
};

// 元组
let nameNumber: [string, number];
nameNumber = ['Jenny', 221345];
