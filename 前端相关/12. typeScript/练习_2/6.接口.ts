// 示例 A 内联注解
declare const myPoint: { x: number; y: number };

// 示例 B 使用接口形式
interface Point {
  x: number;
  y: number;
}
declare const myPoint1: Point;

// 好处在于，如果有人创建了一个基于 myPoint 的库来添加新成员, 那么他可以轻松将此成员添加到 myPoint 的现有声明中
interface Point {
  z: number;
}
