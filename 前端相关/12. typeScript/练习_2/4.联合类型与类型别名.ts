type Texts = string | { text: string };
type Coordinates = [number, number];
type Callback = (data: string) => void;
