enum AnimalFlags {
  None = 0,
  HasClaws = 0,
  CanFly = 1,
  EatsFish = 2,
  Endangered = 3,
}

export enum EvidenceTypeEnum {
  UNKNOWN = "",
  PASSPORT_VISA = "passport_visa",
  PASSPORT = "passport",
  SIGHTED_STUDENT_CARD = "sighted_tertiary_edu_id",
  SIGHTED_KEYPASS_CARD = "sighted_keypass_card",
  SIGHTED_PROOF_OF_AGE_CARD = "sighted_proof_of_age_card",
}

console.log(AnimalFlags.HasClaws);
console.log(EvidenceTypeEnum.PASSPORT);

// 默认从 0 开始
enum Status4 {
  MASSAGE,
  SPA,
  DABAOJIAN,
}

// 从 1 开始
enum Status5 {
  MASSAGE = 1,
  SPA,
  DABAOJIAN,
}

// 反查
console.log(Status5.MASSAGE, Status5[1]);
