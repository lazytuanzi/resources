package main

// 必须定义包,必须是main

import "fmt"

// 用于打印数据

func main() { //入口函数,必须是main

	var a = 123     //定义变量必须使用，否则报错
	var b = "b"     //使用 var 定义变量 ，同 js
	var c int = 789 // var 变量名 类型 = 表达式 ，类型可以省略
	d := "dd"       // 变量名 := 表达式

	fmt.Print("123")            //不换行输出，输出多值无空格,可以输出变量
	fmt.Println("Hello World!") //换行输出，输出多值有空格,可以输出变量
	fmt.Printf("456")           //不换行输出，输出多值无空格,必须格式化输出，无法直接输出
	fmt.Println(a, b, c, d)
	fmt.Printf("%v", a) //必须格式化输出，无法直接输出

}
