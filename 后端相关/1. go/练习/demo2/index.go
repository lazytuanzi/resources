package main

import (
	"fmt"
)

func getKey() (string, int) {
	// 定义一个函数，返回了2种值  ，第二个()约束了类型
	return "key2", 10
}

func main() {

	var user string //定义变量不赋值，输出则为 空 ，未声明前输出报错
	fmt.Println(user)
	user = "张三"

	var a1, a2 string = "11", "22" // 一次声明多个同类型的遍历
	// a1 = "1"
	// a2 = "2"
	fmt.Println(a1, a2)

	var ( // 一次声明多个不同类型的遍历
		key string = "key"
		val int    = 123
		num        = 10
		// num2        := 10	错误写法

	)
	// key = "key"
	// val = 123
	fmt.Println(key, val, num)

	var _, w = getKey() //调用返回2个值，但是只想要某一个，使用 _ 占位，代替，不获取
	fmt.Println(w)

	const pi = 3.1415926
	fmt.Println(pi)

	const (
		size = iota
		size1
		size2
		_
		p3    = 333
		size3 = iota
		size4
	)
	fmt.Println(size, size1, size2, p3, size3, size4)


}
