package main

import (
	"fmt"
	"strings"
	"unsafe"
	// "github.com/shopspring/decimal"
)

func main() {
	var num int8 = 12
	fmt.Printf("num=%v 类型：%T", num, num)
	fmt.Println("----")
	fmt.Println(unsafe.Sizeof(num)) //查看在内存中所占大小

	var num2 int16 = 13
	// fmt.Println(num + num2) 不同长度的 int 不能直接运算
	fmt.Println(num2 + int16(num2))
	fmt.Println(num + int8(num2))

	num3 := 45
	fmt.Println(num3)
	fmt.Printf("num3=%v\n", num3) //原样输出
	fmt.Printf("num3=%d\n", num3) //10进制输出
	fmt.Printf("num3=%b\n", num3) //2进制输出
	fmt.Printf("num3=%o\n", num3) //8进制输出
	fmt.Printf("num3=%x\n", num3) //16进制输出

	var f float32 = 3.12
	fmt.Printf("f=%v---%f\n", f, f) // %f 保留6位小数点
	fmt.Println(unsafe.Sizeof(f))   //4

	var f2 float64 = 3.12
	fmt.Printf("d2=%v---%.3f\n", f2, f2) //%.3f 保留3位小数点
	fmt.Println(unsafe.Sizeof(f2))       //8

	fmt.Println(3.14e-2) //3.14 除以 10的2次方
	fmt.Println(3.14e2)  //3.14 乘以 10的2次方

	m1 := 8.2
	m2 := 3.8
	fmt.Println(m1 - m2) //4.3999999999999995
	// fmt.Println(decimal.NewFromFloat(m1).Add(decimal.NewFromFloat(m2)))

	fmt.Println(float64(10))
	fmt.Println(int8(float64(10)))

	var bol bool
	fmt.Println(bol, true)

	fmt.Println("D:\\Study\\2021_study\\classify\\9.Go入门")
	fmt.Println("q\nw")
	fmt.Println(`
	q\
	sdf
	sdf
	`)

	fmt.Println(len("qwe"))
	fmt.Println("a" + "b")
	fmt.Println(fmt.Sprintf("%v %v", "a", "b"))
	fmt.Println(strings.Join(strings.Split("qe,w,r", ","), "**"))
	fmt.Println(strings.Contains("qe,w,r", "e"))
	fmt.Println(strings.Index("qe,w,r", "e"))
	fmt.Println(strings.LastIndex("qe,w,r", "e"))
	fmt.Println(strings.HasPrefix("asd", "w"))

}
