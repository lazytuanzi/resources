import java.io.*;
import java.util.logging.FileHandler;

public class Demo12_main {
    public static void main(String[] args)   {
//        异常处理
//        int a = 10 / 0;
//        System.out.println(a);  报错
        try {
            int a = 10 /0;
            System.out.println("输出错误信息" + a);
        }catch (Exception e){
            System.out.println(e);
//            return;
        }finally {
            System.out.println("总是执行");

        }
        System.out.println("try catch后");

        {
//            throws方法抛出错误
//            show();
            System.out.println("throws后");

        };
        {
//            file 类
//            将xx封装成 file对象
//            构造方法3种写法
            File fil = new File("D:\\Study\\2021_study\\classify\\1.Java\\case-2\\test_IO\\io.txt");
            System.out.println("file");

            System.out.println(fil);
            System.out.println("\\");
//2
            File fil2 = new File("D:\\Study\\2021_study\\classify\\1.Java\\case-2\\test_IO\\","io.txt");
            System.out.println(fil2);

            File fil3 = new File("D:\\Study\\2021_study\\classify\\1.Java\\case-2\\test_IO\\");
//3
            File fil4 = new File(fil3,"io.txt");

            System.out.println(fil4);


            try {
                //            创建文件
                File fil5 = new File(fil3,"io2.txt");
                 boolean flg =  fil5.createNewFile();
                System.out.println(flg+"创建文件结束");

            //创建路径
                File fil6 = new File(fil3,"\\test_fil");
                boolean flg2 =  fil6.mkdir();
                System.out.println(flg2+"创建文件夹结束");

//                创建对路径
                File fil7 = new File(fil3,"\\test_fil2\\a\\b");
                boolean flg3 =  fil7.mkdirs();
                System.out.println(flg3+"创建多级文件夹结束");

//                判断xx是否为文件夹
                boolean flg4 = fil7.isDirectory();
                System.out.println(flg4);

                //                判断xx是否为文件
                boolean flg5 = fil6.isFile();
                System.out.println(flg5);

                System.out.println(fil6.exists());

                {
//                    FileReader 创建字符流 读取对象
                    Reader rea = new FileReader("D:\\Study\\2021_study\\classify\\1.Java\\case-2\\test_IO\\io.txt");

//                    int data = rea.read();
////                    读取方法
//                    System.out.println(data);

                    int ch;
                    while ((ch = rea.read()) != -1){
//                        ch = rea.read();
//                    优化读取方法
                        System.out.println(ch);
                    }


//                    读取到数组中
                    Reader rea2 = new FileReader("D:\\Study\\2021_study\\classify\\1.Java\\case-2\\test_IO\\io.txt");
                    char[] chs = new char[3];
//                    int len = rea2.read(chs);
//                    System.out.println(len);
//                    System.out.println(chs);
//
//                    int len2 = rea2.read(chs);
//                    System.out.println(len2);
//                    System.out.println(chs);
//                    由于数组长度不够所以覆盖第一次读取的值
                    int len3 ;
                    while ((len3 = rea2.read(chs)) != -1){

                        String s = new String(chs,0,len3);
                        System.out.println(s+"     while s");
                    }
                    {
//                    写入
                        Writer wri = new FileWriter("D:\\Study\\2021_study\\classify\\1.Java\\case-2\\test_IO\\io.txt");
                        wri.write("写水电费是");
                        System.out.println("写入完成");
                    }

                    rea.close();

                }



            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
    public static void show() throws Exception {
        int a2 = 10 / 0;
        System.out.println(a2);
    }
}
