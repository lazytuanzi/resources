/**
 * @author 41947
 */
public class Demo7_parent {
    private String  name;
    private int age;
    int size = 20;


    public Demo7_parent() {
    }

    public Demo7_parent(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public void  testFn(){
        System.out.println("父fn");
    }
}
