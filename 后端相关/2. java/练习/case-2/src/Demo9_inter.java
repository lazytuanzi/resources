public interface Demo9_inter {
//    定义 公有 静态 常量 ，不用写任何修饰符，默认携带
    int num = 10;


//成员方法 ，默认 有 public abstract
    void sake();

    public static void go(){
        System.out.println("这是go静态方法");
    }
    public default void to(){
        System.out.println("这是to默认方法");
    };

//    private void com(){
////        JDK9以后才有
//        System.out.println("这是com私有方法");
//
//    }

//    public Demo9_inter(){
//        接口中没有构造方法
//    }
}
