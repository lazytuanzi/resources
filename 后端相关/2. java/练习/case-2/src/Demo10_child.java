import java.util.Objects;

public class Demo10_child {
    private int id ;
    private String name;
    private int score;
    public Demo10_child() {
    }

    public Demo10_child(int id, String name, int score) {
        this.id = id;
        this.name = name;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(int score) {
        this.score = score;
    }

//    重写 toString 、equals

    @Override
    public String toString() {
        return "Demo10_child{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Demo10_child that = (Demo10_child) o;
        return id == that.id &&
                score == that.score &&
                name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, score);
    }
}
