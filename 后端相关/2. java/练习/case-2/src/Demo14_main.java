
import java.sql.Connection;
import java.sql.DriverManager;
class Demo14_mian{  //类名
    public static void main(String[] args){
        String URL="jdbc:mysql://localhost:3306/test?serverTimezone=UTC"; //DataBaseName是要连接的数据库的名称  数据库名
        String UserName = "root";   //其中UserName是MySQL的用户名称  用户名
        String PassWord = "123456";      //账户密码
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("成功加载驱动！");
            Connection con = null;
            con = DriverManager.getConnection(URL,UserName,PassWord);
            if(con!=null){
                System.out.println("连接数据库成功！");
                System.out.println(con);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
