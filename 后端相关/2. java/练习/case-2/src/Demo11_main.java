import java.util.*;

/**
 * @author 41947
 */
public class Demo11_main {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("??");
        list.add(168);

        Demo11_stud st = new Demo11_stud("阿离",18);
        list.add(st);

        for (Object o : list) {
            System.out.println(o);
        }

        //            迭代器

//        Iterator it =  list.iterator();
//        while (it.hasNext()){
//            Object s = it.next();
//            System.out.println(s);
//            if ("??".equals(s) ){ //解决特殊情况 ，空指针异常
//                list.add("添加");//报错 ，并发修改
//            }
//        }
        ListIterator it =  list.listIterator();
        while (it.hasNext()){
            Object s = it.next();
            System.out.println(s);
            if ("??".equals(s) ){ //解决特殊情况 ，空指针异常
                it.add("添加2");
            }
        }
        System.out.println(list);


        {
            List<String> list2 = new ArrayList<>();
            list2.add("q");
            list2.add("w");
            list2.add("2");
//            list2.add(2); 报错 类型转换异常，只能存入 String类型

            for (String s : list2) {
                System.out.println(s);
            }


        }
        {
            List<Integer> list3 = new ArrayList<>();
            list3.add(15);
            list3.add(5);
            list3.add(51);
            System.out.println("操作前"+list3);

            Integer max = Collections.max(list3);
            System.out.println(max);



            Collections.shuffle(list3);
            System.out.println(list3);
        }
        {
            Set<Integer> set = new HashSet<>();
            set.add(18);
            set.add(16);
            set.add(16);
            System.out.println(set);

            Iterator<Integer> its =  set.iterator();
            while (its.hasNext()){
//                通过迭代器遍历
                Integer ss = its.next();
                System.out.println(ss);

            }

            for (Integer s : set) {
//                增强for实现
                System.out.println(s);

            }
        }

        {
            Map<String, String> map = new HashMap<>();
            map.put("a","A");
            map.put("a1","A1");
            map.put("a2","A2");
            System.out.println(map);

            String gets =  map.get("a");
//            获取值
            System.out.println(gets);

            for (String s : map.keySet()) {
//                通过增强for遍历
                System.out.println(s);
                System.out.println(map.get(s));

            }

            Set<String> keys = map.keySet();
            Iterator<String> itm =  keys.iterator();
            while (itm.hasNext()){
//                通过迭代器遍历
                String ss = itm.next();
                System.out.println(ss);

            }

        }



    }
}
