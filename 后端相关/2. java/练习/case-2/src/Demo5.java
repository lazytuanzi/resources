/**
 * @author 41947
 */
public class Demo5 {

    public static int num(int a, int b){
        return a+b;
    }
    public static void fn1(int u){
        System.out.println(u);
    }
    public static void fn1(char u){
        System.out.println(u);
    }
    public static void main(String[] args) {
        System.out.println("方法");
        int q = num(1,2);
        System.out.println(q);

        fn1('f');
        fn1(123);
//        方法重载
//---------------------------------------------------------
        {
            System.out.println("数组");
            int[] arr = {11,33,99,77};
            System.out.println(arr[2]);
            arr[1] = 22;

            for (int i : arr) {
                System.out.println(i + "循环");
            }
        }
        {
//            空指针异常
            int[] arr1 = new int[2];
            arr1[0]= 1;
            arr1[1]= 2;
//            arr1 = null;
            System.out.println(arr1[1]);


        }


    }


}
