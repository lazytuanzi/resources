import com.sun.javaws.IconUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * @author 41947
 */
public class Demo10_main {
    public static void main(String[] args) {
        Object obj1 = new Object();
        Object obj2 = new Object();

        int code1  = obj1.hashCode();
        System.out.println(code1);

        Class  cla = obj1.getClass();
        System.out.println(cla);

        String str = obj1.toString();
        System.out.println(str);

        boolean bl = obj1.equals(obj2);
        System.out.println(bl);

        {
//            重写 toString  equals hashCode函数 的 使用
            Demo10_child ch = new Demo10_child(1,"亚",98);
            Demo10_child ch2 = new Demo10_child(1,"亚",98);

            System.out.println(ch.toString());
            System.out.println(ch.equals(ch2));


        }
        {
//            .hasNextXxx() , 判断是否还有下一个输入项 ，其中 Xxx 可以是任意基本类型，返回结果为布尔值
//            .nextXxx() , 获取下个输入项 ，其中 Xxx 可以是任意基本类型，返回对应类型的数据
//            String nextLine() ,获取下一行数据，以换行符 作为 结束字符
//            String next() , 获取下一个输入项，以空白符作为 结束字符
            Scanner sc = new Scanner(System.in);
            System.out.println("请输入整数");
//            if(sc.hasNextInt()){
//                int num = sc.nextInt();
//                System.out.println(num);
//            }else{
//                System.out.println("输入错误");
//            }

            String str2 = sc.next();
            System.out.println(str2);


        }
        {
//            String类
            byte[] bys = {97, 100, 99};
            String strs = new String(bys);
            System.out.println(strs);

            char[] chas = {'q','w','e'};
            String s1 = new String(chas);
            System.out.println(s1);

            String s2 = "okmngt";
            System.out.println(s2);

//            equals() : 判断 2个字符串是否相等 ，大小字符区分
//            equalsIgnoreCase() :判断 2个字符串是否相等 ，大小字符不区分
//            startsWith() : 判断 是否 以 某字符 开头
//            isEmpty()
            String aa = "aa";
            String bb = "AA";

            boolean bl2 = aa.equals(bb);
            System.out.println(bl2);

            boolean bl3 = aa.equalsIgnoreCase(bb);
            System.out.println(bl3);

            boolean bl4 = aa.startsWith("a");
            System.out.println(bl4);
            boolean bl5 = aa.isEmpty();
            System.out.println(bl5);

        }
        {
            StringBuilder sbf = new StringBuilder();
            sbf.append("qwer");
            System.out.println(sbf);

            StringBuilder sbf2 = new StringBuilder("okmngt");
            System.out.println(sbf2);

        }
        {
//            时间对象
            Date d = new Date();
            System.out.println(d);
            System.out.println(d.getTime());

            Date d2 = new Date(123456987L);
            System.out.println(d2);



        }
        {
            Calendar c = Calendar.getInstance();
            System.out.println(c);
//            获取
            int year = c.get(Calendar.YEAR);
            System.out.println(year);
//              设置
            c.set(2066,2,2);
            Calendar c2 = Calendar.getInstance();
            System.out.println(c2);



        }
        {
            int as = 10;
            Integer il = new Integer(as);
            System.out.println(il);
            int bw = il.intValue();
            System.out.println(bw);
//------------
            Integer ok = 60;
            int  ok2 = ok;
//            -------------
            String hg = "99";
            int numk = Integer.parseInt(hg);
            System.out.println(numk+100);

            String str0 = "字符串";

            char[] newstr = str0.toCharArray();
//            字符串 转 char 类型
            System.out.println(newstr);

            char newstr1 = str0.charAt(1);

            System.out.println(newstr1);




        }


    }
}
