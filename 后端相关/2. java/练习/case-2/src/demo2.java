/**
 * 文档注释
 */

public class demo2 {
    /**
     * main方法是程序的主入口
     */

    public static void main(String[] args) {
        //这是main方法的主体，我们实现的功能都要谢在这里
        System.out.println("java核心");
        //这是输出语句

        /*
        *
        * 多行注释
         */
//-------------------------------------------------------------------
        System.out.println("abc");
        System.out.println(123);
        System.out.println(1.23);
        System.out.println("a");
        System.out.println(true);

//-------------------------------------------------------------------
        int number=12;
        System.out.println(number);

        long l = 10000000000L;
        float f = 10.3F;
        double d =10.23;
        char c = 'c';

        int aa=15,bb=16;
//-----------------------------------------------------------
        {
            int a= 10;
            byte b = 1;
            int cc = a+ b;
            //隐式转换
            System.out.println(cc);

            byte dd = (byte)(a + b);
            //强制类型转换
            System.out.println(dd);

        }
        {
            double pi = 3.14;
            int o = (int)pi;
            //转化丢失精度
            System.out.println(o);



        }
//----------------------------------------------



    }
}
