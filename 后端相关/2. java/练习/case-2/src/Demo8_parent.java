

public abstract class Demo8_parent {
    private String name="小亚";
    int age = 114;
    // 抽象类
    public Demo8_parent() {
    }
    public Demo8_parent(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public abstract void eat();
}

//public class Dem08_parent  {
//    private String name="小亚";
//    int age = 114;
//
//    public Dem08_parent() {
//    }
//    public Dem08_parent(String name) {
//        this.name = name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//
//    public void eat(){
//        System.out.println("吃饭");
//    }
//}
