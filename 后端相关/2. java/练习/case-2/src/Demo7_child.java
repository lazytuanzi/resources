public class Demo7_child extends Demo7_parent {

    public Demo7_child() {
    }

    public Demo7_child(String name, int age, String like) {
        super(name, age);
        this.like = like;
    }

    private String  like;
    int size = 10;

    public int getSize() {
        int size = 5;
        System.out.println(size);
        System.out.println(this.size);
        System.out.println(super.size);

        return size;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }
    @Override
    public void  testFn(){
//        super.testFn();这是父类的fn
        System.out.println("子fn");
    }
}
