public class Demo8_child2 extends Demo8_parent {
    int age = 31;

    public final static String X = "x?";
//    定义公共的静态常量
    public final static int s = 159;

    public static void show(){
//        静态方法
        System.out.println(s);
    }

    public Demo8_child2() {
    }
    public Demo8_child2(String name) {
        super(name);
    }
    @Override
    public void eat(){
        System.out.println(getName()+"吃奶酪");
    }
}
