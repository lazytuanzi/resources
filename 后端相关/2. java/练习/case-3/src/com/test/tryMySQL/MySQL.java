package com.test.tryMySQL;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MySQL {
    static Connection conn;
    static Statement st;
    public static String Driver = "com.mysql.cj.jdbc.Driver";   //驱动
    //数据库 地址
    public static String jdbcurl = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone = GMT";
    public static String user = "root";     //数据库 用户名
    public static String pwd = "123456";    //数据库 密码


    /**
     * 连接数据库
     *
     * @return
     */
    public Connection getConnection() {
        Connection con = null;    //创建用于连接数据库的Connection对象
        try {

            Class.forName(Driver);// 加载Mysql数据驱动
            System.out.println("成功加载驱动！");
            con = DriverManager.getConnection(jdbcurl, user, pwd);// 创建数据连接
//            System.out.println("连接成功");
        } catch (Exception e) {
            System.out.println("数据库连接失败" + e.getMessage());
        }
        return con;    //返回所建立的数据库连接
    }

    /**
     * 增删改 操作
     *
     * @param sql   SQL语句
     * @param setls PreparedStatement 类，执行前请将参数传入类
     * @return
     */
    public Boolean iudsql(String sql, List<Object> setls) {
        Connection conn = getConnection();    // 首先要获取连接，即连接到数据库
        PreparedStatement stmt;
        try {
            //创建用于执行静态sql语句的Statement对象，st属局部变量

            stmt = conn.prepareStatement(sql);

            // 执行插入操作的sql语句，并返回插入数据的个数
            if (setls != null && setls.size() > 0) {
                for (int i = 1; i <= setls.size(); i++) {
                    stmt.setObject(i, setls.get(i - 1));
                }
            }
            int count = stmt.executeUpdate();
            System.out.println("操作成功");
            conn.close();    //关闭数据库连接
            return true;
        } catch (SQLException e) {
            System.out.println("插入数据失败" + e.getMessage());
        }
        return false;
    }

    /**
     * 查询
     *
     * @param sql   SQL
     * @param setls
     * @return
     */
    public List<HashMap<String, Object>> selectall(String sql, List<Object> setls) {
        Connection conn = getConnection();    // 首先要获取连接，即连接到数据库
        PreparedStatement stmt;
        try {
            List<HashMap<String, Object>> ls = new ArrayList<>();
            //创建用于执行静态sql语句的Statement对象，st属局部变量
            stmt = conn.prepareStatement(sql);
            // 执行插入操作的sql语句，并返回插入数据的个数
            if (setls != null && setls.size() > 0) {
                for (int i = 1; i <= setls.size(); i++) {
                    stmt.setObject(i, setls.get(i - 1));
                }
            }
            ResultSet rs = stmt.executeQuery();    //执行sql查询语句，返回查询数据的结果集
            ResultSetMetaData rsmd = rs.getMetaData();
            while (rs.next()) {
                HashMap<String, Object> ha = new HashMap<>();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    if (rsmd.isNullable(i + 1) == 0) {
                        String columnLabel = rsmd.getColumnLabel(i + 1);
                        ha.put(columnLabel, rs.getObject(columnLabel));
                    }
                }
                ls.add(ha);
            }
            conn.close();    //关闭数据库连接
            return ls;
        } catch (SQLException e) {
            System.out.println("查询数据===" + e.getMessage());
        }
        return null;
    }




//    /**
//     * 查找用户
//     *
//     * @param uname
//     * @return
//     */
//    public Boolean checkUser(String uname) {
//        conn = getConnection();
//        PreparedStatement stmt = null;
//        try {
//            String sql = "SELECT * FROM consumer WHERE name = ?";
//            stmt = conn.prepareStatement(sql);
//            stmt.setString(1, uname);
//            ResultSet rs = stmt.executeQuery();
//            while (rs.next()) {
//                return true;
//            }
//            conn.close();
//        } catch (SQLException e) {
//            System.out.println("查找失败");
//        }
//        return false;
//    }
//
//    /**
//     * 注册用户
//     *
//     * @param name
//     * @param pwd
//     * @return
//     */
//    public Boolean register(String name, String pwd) {
//        if (checkUser(name)) {
//            return false;
//        }
//        conn = getConnection();
//        PreparedStatement stmt = null;
//        try {
//            String sql = "INSERT INTO consumer(name,passwrod) VALUES (?,?)";
//            stmt = conn.prepareStatement(sql);
//            stmt.setString(1, name);
//            stmt.setString(2, pwd);
//            int rs = stmt.executeUpdate();
//            if (rs > 0) {
//                return true;
//            }
//            conn.close();
//        } catch (SQLException e) {
//            System.out.println("注册失败");
//        }
//        return false;
//    }
}

