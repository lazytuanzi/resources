package com.test.servlet;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class Servlet_1 implements Servlet{
    public Servlet_1() {
        System.out.println("1：构造器,只有在第一次创建时调用");
    }

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("2:init，只有在第一次创建时调用");

        //-----------------------------------
//        1. 可以获取servlet的别名 servlet-name
        System.out.println(servletConfig.getServletName());
//        2. 获取 初始化参数 init-param
        System.out.println(servletConfig.getInitParameter("name"));

//        3. 获取 servletContext对象
        System.out.println(servletConfig.getServletContext());




        //-----------------------------------


    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("3:service,专门用来处理请求和响应的，每次访问都调用");
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String methods = httpServletRequest.getMethod();
//        System.out.println(methods);
        if ("GET".equals(methods)){
            System.out.println("get");
            doGet();
        }else if ("POST".equals(methods)){
            System.out.println("post");
            doPost();
        }

    }
    public void doGet(){
    //处理get
    }
    public void doPost(){
    //处理post
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {
        System.out.println("4:destroy，web工程停止才会执行");

    }
}
