package com.test.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class RequesrAPIServlet_5 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//       请求转发
        String Ya =  req.getParameter("ya");
//    接收参数

       req.setAttribute("key","ok");
//       告知servlet_2有通过


        RequestDispatcher reqs =  req.getRequestDispatcher("/hello2");
//       获取路径
        reqs.forward(req,resp);
//        向前进，走到servlet_2

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
//        设置请求体字符集，解决post请求的中文乱码

        //        getRequestURI()             获取请求资源路径
        System.out.println(req.getRequestURI());
//        getRequestURL()             获取绝对路径
        System.out.println(req.getRequestURL());

//        getRemoteHost()            获取客户端ip
        System.out.println(req.getRemoteHost());

//        getHeader()                 获取请求头
        System.out.println(req.getHeader("Host"));
        System.out.println(req.getHeader("User-Agent"));
//        这里有很多属性可以获取

//        getParameter()              获取请求参数
        System.out.println(req.getParameter("ya"));

//        getParameterValuse()        获取请求参数（多值时使用）
        String[] Datas = req.getParameterValues("ck");
        System.out.println(Arrays.asList(Datas));
//        getMethod()                 获取请求方式
        System.out.println(req.getMethod());
    }
}
