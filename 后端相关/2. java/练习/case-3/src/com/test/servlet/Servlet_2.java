package com.test.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Servlet_2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("servlet2 doGet");
        System.out.println(getServletName());
//-------------------------------------------------------------
//        请求转发 到这里
        String Ya =  req.getParameter("ya");
//    接收参数
        Object key = req.getAttribute("key");
//       判断是否经过 之前
        System.out.println(key);
        System.out.println("servlet2 处理自己的业务");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("servlet2 doPost");

    }
}
