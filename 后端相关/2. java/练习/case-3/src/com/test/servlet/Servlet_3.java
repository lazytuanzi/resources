package com.test.servlet;

import com.test.tryMySQL.MySQL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


public class Servlet_3 extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("p3");
//       重定向
//        response.setStatus(302);
//        设置响应状态码
//        response.setHeader("Location","http://localhost:8088/hello2");
//        设置响应头

        response.sendRedirect("http://localhost:8088/hello2");
//        一句话完成重定向

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        获取数据库
        MySQL d =new MySQL();
        List<Object> arrs = new ArrayList<>();
        arrs.add("0");
        arrs.add("9");
        arrs.add("2020-11-25");
        String sql = "insert INTO ceshi_biao(ceshi_title,ceshi_author,submission_date) VALUES (?,?,?)";
        d.iudsql(sql,arrs);

//        d.getConnection();

//        -------------------------------------------------------------

//        response.setCharacterEncoding("UTF-8");
//        设置返回字符集
//        response.setHeader("Content-Type","text/html;charset=UTF-8");
//        通过响应头设置浏览器字符集

        response.setContentType("text/html;charset=UTF-8");
//        一句话解决乱码

        System.out.println("g3");
        PrintWriter write =  response.getWriter();
        write.println("{data:'返回信息数据',arr:[1,8,5]}");
    }
}
