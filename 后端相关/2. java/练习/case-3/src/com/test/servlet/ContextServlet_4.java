package com.test.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ContextServlet_4 extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("44");

        ServletContext context = getServletConfig().getServletContext();
//        1. 获取web.xml 中配置的上下文参数 content.param
        System.out.println(context.getInitParameter("data"));
//        2. 获取当前的工程路径
        System.out.println(context.getContextPath());

//        3. 获取工程部署后在服务器硬盘上的绝对路径
        System.out.println(context.getRealPath("/"));


//        4. 像Map一样存取数据
        context.setAttribute("age","18");
        System.out.println(context.getAttribute("age"));
        context.removeAttribute("age");



    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("442");
    }
}
