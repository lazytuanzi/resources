public class demo3 {
    public static void main(String[] args) {
        byte y = 1;
        short s=y;
        int i=y;
        System.out.println(i);
//        将小范围的数据类型自动提升为大范围的数据类型,自动转换

        byte x = 1;
        int j = x + y;
//        byte z = x + y;//报错 x+y(int类型)
        System.out.println(j);
//        所有长度低于int的类型（byte、short、char）在运算之后结果将会被提升为int型

        double d = 2.5;
        //int类型和double类型运算，结果	是double类型
        //int类型会提升为double类型
        double e = d+i;
        System.out.println(e);

        {
            int ii =1;
            byte ss = (byte) ii;
            System.out.println("s=" + ss);
//            转换格式：
//            数据类型 变量名 = （数据类型）被转数据值；
            //ss = ss + 1；//编译失败
            ss = (byte) (ss + 1);//编译成功
            System.out.println(ss);



        }
        {
            int i1=(int)1.5;
            System.out.println("i=" + i1);
            int ww=32777;
            short w=(short)ww;
            System.out.println("w="+w);
//            浮点转成整数，直接取消小数点，可能造成数据损失精度。

//            int 强制转成 short 砍掉2个字节，可能造成数据丢失。


        }
        {
            int i2 = 1234;
            //计算结果是1000
            System.out.println(i2/1000*1000);


            int aa1 = 4+(5+16)/3*2%4-4;
            System.out.println("a" + aa1);
//--------------------------------------
            int i0=1,j0;
            j0 = i0++;
            System.out.println(j0);
            System.out.println(i0);



        }
        method();
    }
    //定义方法，被main方法调用
    public static void method() {
        System.out.println("自己定义的方法，需要被main调用运行");
    }


}
