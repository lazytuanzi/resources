import test1.publicpackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class demo7 {
    public static void main(String[] strs){
        Scanner scan = new Scanner(System.in);
        //         从键盘接收数据

//        System.out.println("next方式接收：");
//        if (scan.hasNext()) {
//            //         判断是否还有输入
//
//            String str1 = scan.next();
//            //         next方式接收字符串
//
//            System.out.println("输入的数据为：" + str1);
//        }else{
//            System.out.println("输入的数据错误");
//        }
//-----------------------------------------------------

        System.out.println("nextLine方式接收：");
        if(scan.hasNext()){
            String str1=scan.nextLine();
            System.out.println("输入的数据为：" + str1);
        }
        scan.close();

        {

            Random random=new Random();
            for(int i=0;i<3;i++){
                int j=random.nextInt(10);
                System.out.println("j="+j);
            }



        }
//--------------------------------------------------------------------------
        {
            String []strs12={"0","1","2","3","4","5","6","7","8","9"
                    ,"A","B","C","D","E","F"};
            Random random=new Random();
            String value12="";
            for(int i=0;i<4;i++){
                int j=random.nextInt(strs12.length);
                value12 += " " +strs12[j];
            }
            System.out.println(value12);

        }
//-----------------------------------------------------------------------
        {
            List<String> lsq=new ArrayList<>();
            lsq.add("111");
            lsq.add("222");
            lsq.add("333");

            System.out.println(lsq.get(0));
            System.out.println(lsq.size());
            System.out.println(lsq.remove(2));
            System.out.println(lsq.size());

            for(int i=0;i<lsq.size();i++){
                System.out.println(lsq.get(i));
            }

        }


    }
}
