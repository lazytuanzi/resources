public class demo2 {
    // 静态常量
    public static final double PI = 3.14;

    // 声明成员常量
    final int y = 10;

    public static void main(String[] args) {
        // 声明局部常量
        double x = 3.3;

        {
            Object i = null;
            // null 表示空
            String b;
            int k;

            Boolean t = true;

            System.out.println(i);
            System.out.println(t);
            System.out.println("输出");
        }
//        System.out.println(i); 报错 ， {}作用域

    }


}
