
/**
 * @author 41947
 */
public class demo1 {
    public static  void main2(String [] args){
        byte number1 = 10;

        //     byte   整数
        //     最小值是 -128（-2^7）；
        //      最大值是 127（2^7-1）；

        System.out.println(number1);
        short number3 = 10000;
        //     short   整数
        // 最小值是 -32768（-2^15）；
        //  最大值是 32767（2^15 - 1）；
        System.out.println(number3);

        // int  long  整数

        float f1 = 234.5f;

        double d1 = 123.4;
        System.out.println(f1);

        System.out.println(d1);

        //float 数据类型是单精度

        //double 数据类型是双精度

        boolean one = false;
        //boolean数据类型
//        System.out.println(one);

        char letter = 'A';
        //char类型是一个单一的 16 位 Unicode 字符

        System.out.println(letter);



    }

}
