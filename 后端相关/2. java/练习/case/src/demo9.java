public class demo9 {
    public demo9(String name){
//        构造方法
        this.name=name;
    }
    public demo9(int ages , int b){
//        构造方法 重载
        this.ages=ages+b;
    }

//    ------------------------------
//    封装

    private String name;
    private int ages;

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void showName(){
        System.out.println(name);
    }
    public void showName(String name){
        System.out.println(name);
    }
//------------------------------
//    重载
    public void over(String name,int age){

    }
    public void over(String name){
        System.out.println(name);

    }
    public void over(int age){

    }

    public static void main(String[] args) {
        demo9 ove = new demo9("name111");
        ove.showName("name22");
        ove.showName();
//        重载的使用,根据传入的参数不同 返回 不同的结果


    }

}

