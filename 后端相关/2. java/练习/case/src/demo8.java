import java.util.Arrays;

public class demo8 {
    public static void main(String[] strs){
        String str = "hello word";
        String strs2 = new String("hello word");
        System.out.println(str);
        System.out.println(strs2);

        {

            String a="aa",b="bb";

            System.out.println(a.equals(b));
            System.out.println(a.length());

        }
        {
            int[] arr78 = {2,34,35,4,657,8,69,9};
            // 打印数组,输出地址值
            // [I@2ac1fdc4
            System.out.println(arr78);
            // 数组内容转为字符串
            String s = Arrays.toString(arr78);
            // 打印字符串,输出内容
            // [2, 34, 35, 4, 657, 8, 69, 9]
            System.out.println(s);

        }
        {
            int xxx = Math.abs(-4);
            System.out.println(xxx);

        }

    }
}
